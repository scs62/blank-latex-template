TARGET := awesome-paper.pdf
SOURCE := $(TARGET:.pdf=.tex)
LATEXRUNEXE := latexrun/latexrun
BUILDDIR := build
LATEXRUN := $(LATEXRUNEXE) \
	-W no-fontspec \
	--latex-cmd="lualatex" \
	--latex-args="-shell-escape" \
	--bibtex-cmd="biber" \
	-O $(BUILDDIR)

.PHONY: all
all: $(BUILDDIR) git-submodules $(BUILDDIR)/count.log $(BUILDDIR)/gitversion.log $(TARGET)
	@echo -n "word count: "
	@cat $(BUILDDIR)/count.log

.PHONY: diff
diff: $(TARGET:.pdf=-diff.pdf)

$(BUILDDIR):
	mkdir -p $(BUILDDIR)

.PHONY: git-submodules
git-submodules:
	@if git submodule status | egrep -q '^[-]|^[+]' ; then \
		echo "INFO: Need to reinitialize or update git submodules"; \
		git submodule update --init --recursive; \
	fi

.PHONY: FORCE
$(BUILDDIR)/count.log: FORCE
	@texcount -brief -sum $(SOURCE) tex/* | tail -n 1 | sed -e 's/:.*//' \
		> $(BUILDDIR)/count.log

$(BUILDDIR)/gitversion.log: $(BUILDDIR) FORCE
	@- ! [ $$(git rev-parse --git-dir) ] && [ $$(git log) ] || \
		git describe --always --dirty=\\textasciitilde{} > $(BUILDDIR)/gitversion.log
	@- [ -s $(BUILDDIR)/gitversion.log ] || echo no-vc > $(BUILDDIR)/gitversion.log

%.pdf: %.tex FORCE
	$(LATEXRUN) $< -o $@

%-screen.pdf: %.pdf
	pdf-crop-margins --absolutePreCrop4 20 20 20 60 --uniform $< -o $@

%-diff.tex: %.tex $(BUILDDIR) git-submodules $(BUILDDIR)/count.log $(BUILDDIR)/gitversion.log FORCE
	git fetch origin $(BEFORE)
	git fetch --tags
	$(eval BFR := $(shell git describe --tags --always origin/$(BEFORE)))
	sed -i "1s/^/$(BFR)-diff-/" $(BUILDDIR)/gitversion.log
	$(eval TMPWORKTREE := $(shell mktemp -d))
	git worktree add $(TMPWORKTREE) $(BFR)
	-latexdiff -c resources/ld.cfg --flatten --math-markup=0 --exclude-safecmd="textcite" --exclude-textcmd="title,section,subsection,subsubsection" \
		$(TMPWORKTREE)/$(SOURCE) $(SOURCE) > $(TARGET:.pdf=-diff.tex)
	rm -rf $(TMPWORKTREE)
	git worktree prune

.PHONY: clean
clean:
	$(LATEXRUN) --clean-all
	rm -rf $(BUILDDIR)
