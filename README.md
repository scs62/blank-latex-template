blank-latex-template
====================

A clean, blank latex template with various packages that you should (probably)
always use (e.g. inputenc, geometry, bable, hyperref, microtype, etc.).

[Rendered as a PDF](https://gitlab.developers.cam.ac.uk/scs62/blank-latex-template/-/jobs/artifacts/master/raw/awesome-paper.pdf?job=compiled_pdf) latest version on master branch.

A list of useful packages and what they do
------------------------------------------

### Fonts
```latex
\usepackage[tt=false, type1=true]{libertine} % nicer than of times new roman
\usepackage[varqu]{zi4} % inconsolata
\usepackage[libertine]{newtxmath} % math letters from Linux Libertine font
\usepackage{courier} % another good monospaced font
\usepackage{fourier} % Utopia fonts (including the mathsy stuff)
\usepackage[charter]{mathdesign} % nice font for monitors and has maths symbols
\usepackage[sfdefault]{quattrocento} % nice sans serif font
\usepackage[fontsize=10.8pt]{scrextend} % non-integer and <10pt font sizes
```

### Customisation
```latex
\usepackage{parskip} % puts flexible vertical space between paragraphs, rather
                     % than an indent at the start of the paragraph.
\usepackage{enumitem} % customize enumerated and itemized lists
\usepackage{graphicx} % for \includegraphics of .eps, .pdf, etc
\usepackage{framed} % put something into a rectangle/box
\usepackage{multicol} % use multiple columns for a section
\input{epsf} % to allow postscript inclusions
\usepackage{quotchap} % nice quotes at the beginning of chapters
\usepackage{blindtext} % insert dummy lorem ipsum text into the document
\usepackage{sectsty} % Allows customisation of subsection fonts
\usepackage[hang,multiple]{footmisc} % multi-footnote refs are comma separated
\usepackage[super]{nth} % \nth{1} => 1st, \nth{2} => 2nd, etc.
\usepackage[en-GB]{datetime2} % format dates and time zones
\DTMlangsetup{ord=raise} % datetime2 raise ordinals in \today
\usepackage{float} % get lstlistings to float
\usepackage{newfloat} % create new float environments e.g. \begin\end{figure}
```

### Source code listings
```latex
\usepackage{listings}
\usepackage[outputdir=build]{minted}
\usemintedstyle{borland}
```

### Maths
```latex
\usepackage{mathtools} % useful & powerful commands, fixing some amsmath quirks
\usepackage{amssymb} % more math symbols
\let\openbox\relax % prevent clash between newtxmath and amsthm
\usepackage{amsthm} % environments for theorems, definitions, corollaries, etc.
\usepackage{bbold} % \mathbb{1} and other black-board bold fonts
\usepackage{tikz} % the one and only
\usetikzlibrary{external} % save images to speed up build times
\tikzexternalize[prefix=figures/]
\usetikzlibrary{tikzmark}
\usetikzlibrary{positioning}
\usetikzlibrary{calc}
\usetikzlibrary{decorations,decorations.pathreplacing}
\usepackage{tikz-cd} % category theory diagrams
\usepackage{forest} % better trees than tikz
\usepackage{proof} % for inference rules (e.g. type theory)
\usepackage{ebproof} % for proof trees
% (see http://www.logicmatters.net/latex-for-logicians/nd/ )
\usepackage{xfrac} % split-level (slanted height) fractions (or subsitution)
```

### Chart plotting
```latex
\usepackage{pgfplots} % awesome plotting
\pgfplotsset{compat=1.13}
\usepackage{pgfplotstable} % tables from csv files
```

### Tables
```latex
\usepackage{booktabs} % nicer tables
\usepackage{adjustbox} % change the alignment of the content of table cells
\usepackage{tabularx} % get tables that have text wrapping
\usepackage{ltablex} % long tabularx, i.e. break across pages
\usepackage{makecell} % get line breaks inside table cells
```

### BibLaTeX
```latex
\RequirePackage[style=american]{csquotes} % make biblatex happy and some auto quote magic
\RequirePackage[backend=biber,style=numeric,sorting=none,maxbibnames=9999]{biblatex}
\addbibresource{refs.bib}
```

### Default packages
```latex
\usepackage{silence} % filter various warning messages for clean build output
\usepackage[margin=2cm]{geometry} % layout of pages letter/a5/landscape; margins
\usepackage[T1]{fontenc} % makes the font work with utf-8 (is this always necessary?)
\usepackage[utf8]{inputenc} % this file should be saved in utf-8
\usepackage[british]{babel} % use british style hyphenation, dates, etc.
\usepackage{microtype} % Subtle adjustments via protrusion and expansion
\usepackage{xcolor} % get color! Dependency for other packages, e.g. hyperref
\usepackage[hidelinks,pdfusetitle]{hyperref} % clickable URLs and cross-references. Also pdf metadata
\usepackage[all]{hypcap} % hyperref to top of image not to caption
\usepackage[noabbrev,nameinlink]{cleveref} % an alternative to \ref{...} that aims to be consistent
```
