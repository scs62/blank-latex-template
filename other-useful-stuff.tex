% Mathematics
\newcommand{\eqdef}{\stackrel{\text{\tiny def}}{=}} % The Comprehensive LaTeX Symbol List

% Compsci
\usepackage[nounderscore]{syntax} % for BNF context-free grammars.
    % Use nounderscore so that graphicx doesn't break with it
\setlength{\grammarindent}{8em}
\usepackage[plain]{algorithm} % write pseudocode
\usepackage[noend]{algpseudocode}
\usepackage{algorithmicx}
\algrenewcommand\alglinenumber[1]{{\footnotesize\code{\textcolor{gray}{#1}}}}
\renewcommand\algorithmicthen{}
\renewcommand\algorithmicdo{}

\lstset{ % listings settings
    numbers=left,
    basicstyle=\small\ttfamily,
    breaklines=true
}

% Solarized colours for lstlistings
\definecolor{solarized@base03}{HTML}{002B36}
\definecolor{solarized@base02}{HTML}{073642}
\definecolor{solarized@base01}{HTML}{586e75}
\definecolor{solarized@base00}{HTML}{657b83}
\definecolor{solarized@base0}{HTML}{839496}
\definecolor{solarized@base1}{HTML}{93a1a1}
\definecolor{solarized@base2}{HTML}{EEE8D5}
\definecolor{solarized@base3}{HTML}{FDF6E3}
\definecolor{solarized@yellow}{HTML}{B58900}
\definecolor{solarized@orange}{HTML}{CB4B16}
\definecolor{solarized@red}{HTML}{DC322F}
\definecolor{solarized@magenta}{HTML}{D33682}
\definecolor{solarized@violet}{HTML}{6C71C4}
\definecolor{solarized@blue}{HTML}{268BD2}
\definecolor{solarized@cyan}{HTML}{2AA198}
\definecolor{solarized@green}{HTML}{859900}
\lstset{
    breaklines=true,
    basicstyle=\footnotesize\ttfamily\color{solarized@base01},
    rulesepcolor=\color{solarized@base03},
    numberstyle=\tiny\color{solarized@base01},
    keywordstyle=\color{solarized@green},
    stringstyle=\color{solarized@cyan}\ttfamily,
    identifierstyle=\color{solarized@base00},
    commentstyle=\color{solarized@base1},
    emphstyle=\color{solarized@red}
}

% xcolor for bar charts
\definecolor{barblue}{HTML}{4F81BD}
\definecolor{barred}{HTML}{C0504D}
\definecolor{bargreen}{HTML}{9BBB59}
\definecolor{barpurple}{HTML}{9F4C7C}

% hyperref url colours
\hypersetup {
    colorlinks,
    urlcolor={blue!50!black},
    linkcolor=black,
    citecolor=black
}

% Include footnotes in the word count:
%TC:macro \footnote [text]
%TC:macro \footnotetext [text]
%TC:macro \footlink [text]
%TC:macro \footlinks [text]
%TC:macro \cquote [text]

% Biblatex stuff
\DeclareNameAlias{sortname}{last-first}
%\setlength\bibitemsep{1.5\itemsep}
%\renewcommand*{\bibfont}{\small}
% only show isbn for books
\AtEveryBibitem{%
  \ifentrytype{book}{% %do nothing
  }{%
    \clearfield{isbn}%
    \clearfield{issn}%
  }%
}
%Only display URL if there is no DOI
\renewbibmacro*{doi+eprint+url}{% \iftoggle{bbx:url}
{\iffieldundef{doi}{\usebibmacro{url+urldate}}{}} {}% \newunit\newblock
\iftoggle{bbx:eprint} {\usebibmacro{eprint}} {}% \newunit\newblock
\iftoggle{bbx:doi} {\printfield{doi}} {}}

% stop biblatex from updating the page header
\defbibheading{proposalReferences}{%
  \section*{#1}%
}

% biblatex use square brackets [except inside square (in which case use round)].
\makeatletter

\newrobustcmd*{\parentexttrack}[1]{%
  \begingroup
  \blx@blxinit
  \blx@setsfcodes
  \blx@bibopenparen#1\blx@bibcloseparen
  \endgroup}

\AtEveryCite{%
  \let\parentext=\parentexttrack%
  \let\bibopenparen=\bibopenbracket%
  \let\bibcloseparen=\bibclosebracket}

\makeatother

% Grammar environment:
\DeclareFloatingEnvironment[
  % the file extension for the file used to create the list:
  fileext   = logr,% don't use log here!
  % the heading for the list:
  listname  = {List of Grammars},
  % the name used in captions:
  name      = Grammar,
  % the default floating parameters if the environment is used
  % without optional argument:
  placement = htp
]{Grammar}

% useful stuff for cateogry theory
\newcommand{\adjointDown}{\mathbin{\rotatebox[origin=c]{90}{$\vdash$}}}
\newcommand{\adjointUp}{\mathbin{\rotatebox[origin=c]{90}{$\dashv$}}}

\newcommand{\verteq}{\rotatebox{90}{$\,=$}}
\newcommand{\equalto}[2]{\underset{\displaystyle\overset{\mkern4mu\verteq}{#2}}{#1}}
\newcommand{\mapsfrom}{\reflectbox{$\;\mapsto\;$}}

% Double-lined fractions, notation for bijective correspondence
% https://tex.stackexchange.com/questions/56003/fraction-with-doubled-line
\newcommand{\Efrac}[2]{%
  \mathchoice
    {\ooalign{%
      $\genfrac{}{}{2.0pt}0{#1}{#2}$\cr%
      $\color{white}\genfrac{}{}{1.0pt}0{\phantom{#1}}{\phantom{#2}}$}}%
    {\ooalign{%
      $\genfrac{}{}{2.0pt}1{#1}{#2}$\cr%
      $\color{white}\genfrac{}{}{1.0pt}1{\phantom{#1}}{\phantom{#2}}$}}%
    {\ooalign{%
      $\genfrac{}{}{2.0pt}2{#1}{#2}$\cr%
      $\color{white}\genfrac{}{}{1.0pt}2{\phantom{#1}}{\phantom{#2}}$}}%
    {\ooalign{%
      $\genfrac{}{}{2.0pt}3{#1}{#2}$\cr%
      $\color{white}\genfrac{}{}{1.0pt}3{\phantom{#1}}{\phantom{#2}}$}}%
}

\newcommand{\efrac}[2]{%
  \mathchoice
    {\ooalign{%
      $\genfrac{}{}{2.0pt}0{\hphantom{#1}}{\hphantom{#2}}$\cr%
      $\color{white}\genfrac{}{}{1.0pt}0{\color{black}#1}{\color{black}#2}$}}%
    {\ooalign{%
      $\genfrac{}{}{2.0pt}1{\hphantom{#1}}{\hphantom{#2}}$\cr%
      $\color{white}\genfrac{}{}{1.0pt}1{\color{black}#1}{\color{black}#2}$}}%
    {\ooalign{%
      $\genfrac{}{}{2.0pt}2{\hphantom{#1}}{\hphantom{#2}}$\cr%
      $\color{white}\genfrac{}{}{1.0pt}2{\color{black}#1}{\color{black}#2}$}}%
    {\ooalign{%
      $\genfrac{}{}{2.0pt}3{\hphantom{#1}}{\hphantom{#2}}$\cr%
      $\color{white}\genfrac{}{}{1.0pt}3{\color{black}#1}{\color{black}#2}$}}%
}

