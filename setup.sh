#!/usr/bin/env bash
set -euo pipefail

debug=
#debug=echo

show_usage() {
    local cmd
    cmd=$(basename "${BASH_SOURCE[0]}")
    echo "Usage: $cmd [OPTION]...
Setup a blank latex template.
Options:
  -h -- display this help and quit
  -d -- dry run - print commands that would be run without running them

  -f NAME -- filename is NAME

  -r / -R -- DO / don't: remove script.sh, other-useful-stuff.tex, and README.md
  -g / -G -- DO / don't: delete the .git folder
  -i / -I -- DO / don't: \`git init\` (only after deleting .git)

  -a / -A -- DO / don't: use mathtools, amssymb, amsthm, and possibly more
  -b / -B -- do / DON'T: use BibLaTeX for a bibliography
  -c / -C -- do / DON'T: use chapters in separate files
  -l / -L -- do / DON'T: use lstlistings for source code
  -m / -M -- do / DON'T: use minted for source code
  -p / -P -- do / DON'T: use chart plotting packages
  -t / -T -- do / DON'T: use booktabs, adjustbox, tabularx, ltablex, makecell"
}

MATCH_START="^\\%\\#\\!\\#setup\\.sh\\:"
MATCH_CLOSE="^\\%\\#\\!\\#\\/setup\\.sh\\:"

main() {
    local filename base_url mathtools remove_setup bibliography chapters
    local chart_plotting lstlistings minted rm_git git_init tables
    filename=
    base_url=
    mathtools=
    remove_setup=
    bibliography=
    chapters=
    lstlistings=
    minted=
    chart_plotting=
    rm_git=
    git_init=
    tables=

    local OPTIND
    while getopts 'hdf:rRgGiIaAbBcCpPlLmMtT' opt; do
        case "$opt" in
            h) show_usage; exit 0 ;;
            d) echo "Dry run!"; debug="echo" ;;
            f) filename="$OPTARG" ;;
            r) [[ $remove_setup = "N" ]]   && echo "Error: not both -R and -r" && exit 1 || remove_setup="Y" ;;
            R) [[ $remove_setup = "Y" ]]   && echo "Error: not both -R and -r" && exit 1 || remove_setup="N" ;;
            g) [[ $rm_git = "N" ]]         && echo "Error: not both -G and -g" && exit 1 || rm_git="Y" ;;
            G) [[ $rm_git = "Y" ]]         && echo "Error: not both -G and -g" && exit 1 || rm_git="N" ;;
            i) [[ $git_init = "N" ]]       && echo "Error: not both -I and -i" && exit 1 || git_init="Y" ;;
            I) [[ $git_init = "Y" ]]       && echo "Error: not both -I and -i" && exit 1 || git_init="N" ;;
            a) [[ $mathtools = "N" ]]      && echo "Error: not both -A and -a" && exit 1 || mathtools="Y" ;;
            A) [[ $mathtools = "Y" ]]      && echo "Error: not both -A and -a" && exit 1 || mathtools="N" ;;
            b) [[ $bibliography = "N" ]]   && echo "Error: not both -B and -b" && exit 1 || bibliography="Y" ;;
            B) [[ $bibliography = "Y" ]]   && echo "Error: not both -B and -b" && exit 1 || bibliography="N" ;;
            c) [[ $chapters = "N" ]]       && echo "Error: not both -C and -c" && exit 1 || chapters="Y" ;;
            C) [[ $chapters = "Y" ]]       && echo "Error: not both -C and -c" && exit 1 || chapters="N" ;;
            l) [[ $lstlistings = "N" ]]    && echo "Error: not both -L and -l" && exit 1 || lstlistings="Y" ;;
            L) [[ $lstlistings = "Y" ]]    && echo "Error: not both -L and -l" && exit 1 || lstlistings="N" ;;
            m) [[ $minted = "N" ]]         && echo "Error: not both -M and -m" && exit 1 || minted="Y" ;;
            M) [[ $minted = "Y" ]]         && echo "Error: not both -M and -m" && exit 1 || minted="N" ;;
            p) [[ $chart_plotting = "N" ]] && echo "Error: not both -P and -p" && exit 1 || chart_plotting="Y" ;;
            P) [[ $chart_plotting = "Y" ]] && echo "Error: not both -P and -p" && exit 1 || chart_plotting="N" ;;
            t) [[ $tables = "N" ]]         && echo "Error: not both -T and -t" && exit 1 || tables="Y" ;;
            T) [[ $tables = "Y" ]]         && echo "Error: not both -T and -t" && exit 1 || tables="N" ;;
            *) echo "Unrecognised option(s) passed"; show_usage; exit 1 ;;
        esac
    done
    shift "$((OPTIND -1))"

    if [[ $rm_git = "N" && $git_init = "Y" ]]; then
        echo 'Cannot git init (-i) and keep .git (-G). Please remove git (-g).'
        exit 1
    fi

    if [[ -z "$filename" ]]; then
        echo -n 'What would you like the filename to be called (without .tex/.pdf extension)? '
        read -r filename
    fi
    $debug mv awesome-paper.tex "$filename.tex"
    $debug sed -i "s/awesome-paper/$filename/" makefile
    $debug sed -i "s/awesome-paper/$filename/" .gitlab-ci.yml
    $debug sed -i "s/awesome-paper/$filename/" resources/add-pdf-links.py
    if ask_if_unset "$remove_setup" "Delete other-useful-stuff.tex?" Y; then
        $debug rm -- "./other-useful-stuff.tex";
    fi
    if ask_if_unset "$remove_setup" "Delete README.md?" Y; then
        $debug rm -- "./README.md";
        if ask_if_unset "" "Create README.md with PDF link?" Y; then
            echo 'What is the base-url of the project? (no trailing slash)'
            echo "\(default: https://gitlab.developers.cam.ac.uk/$USER/$filename\)"
            read -r base_url
            if [ -z "$base_url" ]; then
                url_root=$(git remote get-url origin | sed -En "s#^.*@([^:]+):([^/]+)/.*#\1#p")
                base_url="https://$url_root/$USER/$filename"
            fi
            if [ -z "$debug" ]; then
                $debug echo "$filename
====

[Rendered as PDF]\($base_url/-/jobs/artifacts/master/raw/$filename.pdf?job=compiled_pdf\)
latest version on master branch." > README.md
            fi
        fi
    fi
    if ask_if_unset "$remove_setup" "Delete this setup.sh script?" Y; then
        $debug rm -- "$0";
    fi
    if ask_if_unset "$rm_git" "Do you want to remove the .git folder?" Y; then
        $debug rm -rf "./.git"
        if ask_if_unset "$git_init" "Do you want to initialise a new git repository? (\`git init .\`)" Y; then
            $debug git init .
            $debug rm -rf "./latexrun/"
            $debug git submodule add https://github.com/aclements/latexrun
        fi
    fi
    if ! ask_if_unset "$mathtools" "Do you want to use amsthm, braket?" Y; then
        $debug delete_section "$filename.tex" "Maths"
    fi
    if ! ask_if_unset "$bibliography" "Do you want to use BibLaTeX for a bibliography?" Y; then
        $debug delete_section "$filename.tex" "BibLaTeX"
    fi
    if ! ask_if_unset "$chapters" "Do you want to use chapters in separate files?" N; then
        $debug delete_section "$filename.tex" "Chapters"
        $debug rm -r "./tex"
    fi
    if ! ask_if_unset "$lstlistings" "Do you want to use lstlistings for source code?" N; then
        $debug delete_section "$filename.tex" "listings"
        lstlistings=N
    fi
    if ! ask_if_unset "$minted" "Do you want to use minted for source code?" N; then
        $debug delete_section "$filename.tex" "minted"
        minted=N
    fi
    if [[ "$lstlistings" = "N" && "$minted" = "N" ]]; then
        $debug delete_section "$filename.tex" "Code"
    fi
    if ! ask_if_unset "$chart_plotting" 'Do you want to use pgfplots for chart (graph) plotting?' N; then
        $debug delete_section "$filename.tex" "Chart_Plotting"
    fi
    if ! ask_if_unset "$tables" "Do you want to use table packages: booktabs, adjustbox, tabularx, ltablex, makecell?" N; then
        $debug delete_section "$filename.tex" "tables"
    fi
    $debug sed -i -e "/$MATCH_START/d" -e "/$MATCH_CLOSE/d" "$filename.tex"
}

# Given a filename and an identifier, deletes the text between
# %#!#setup.sh:identifier
# and
# %#!#/setup.sh:identifier
# inclusive of endpoints.
delete_section() {
    sed -i "/$MATCH_START$2\$/,/$MATCH_CLOSE$2\$/d" "$1"
}

# uses the first argument (must be "Y" or "N") if it is non-empty, otherwise
# calls ask()
ask_if_unset() {
    if [[ "$1" = "Y" ]]; then
        return 0;
    elif [[ "$1" = "N" ]]; then
        return 1;
    else
        if ask "${@:2}"; then
            return 0;
        else
            return 1;
        fi
    fi
}

# This is a general-purpose function to ask Yes/No questions in Bash, either
# with or without a default answer. It keeps repeating the question until it
# gets a valid answer.
ask() {
    # https://gist.github.com/davejamesmiller/1965569
    local prompt default reply

    if [ "${2:-}" = "Y" ]; then
        prompt="Y/n"
        default=Y
    elif [ "${2:-}" = "N" ]; then
        prompt="y/N"
        default=N
    else
        prompt="y/n"
        default=
    fi

    while true; do

        # Ask the question (not using "read -p" as it uses stderr not stdout)
        echo -n "$1 [$prompt] "

        # Read the answer (use /dev/tty in case stdin is redirected from somewhere else)
        read -r reply </dev/tty

        # Default?
        if [ -z "$reply" ]; then
            reply=$default
        fi

        # Check if the reply is valid
        case "$reply" in
            Y*|y*) return 0 ;;
            N*|n*) return 1 ;;
        esac

    done
}

is_script_executed() {
    [[ "${BASH_SOURCE[0]}" == "$0" ]]
}

if is_script_executed; then
    main "$@"
fi
